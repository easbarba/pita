/*
 * Pita is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pita is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pita. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <unity.h>

#include "pita_path_tests.h"

#include "../src/pita_path.h"

void
setUp (void)
{
}

void
tearDown (void)
{
}

void
test_pita_path_stem (void)
{
  char *actual = pita_path_stem ("/root/onur/golang.json");
  TEST_ASSERT_EQUAL_STRING ("golang", actual);

  free (actual);
}

void
test_pita_path_suffix (void)
{
  char *actual = pita_path_suffix ("/root/onur/swift.json");
  TEST_ASSERT_EQUAL_STRING ("swift.json", actual);
}

void
test_pita_path_is_empty (void)
{
  TEST_ASSERT_FALSE (pita_path_stem (""));
}

void
test_pita_path_is_cwd_dot (void)
{
  TEST_ASSERT_TRUE (pita_path_is_cwd_dot ("/root/onur/."));
}

void
test_pita_path_is_parent_dot (void)
{
  TEST_ASSERT_TRUE (pita_path_is_parent_dot ("/root/onur/.."));
}
