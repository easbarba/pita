/*
 * Pita is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pita is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pita. If not, see <https://www.gnu.org/licenses/>.
 */

#include <unity.h>

#include "../src/pita_string.h"

void
test_pita_string_starts_with (void)
{
  char *string = "/root/onur/misc.json";
  bool actual = pita_string_starts_with (string, "/root/o", 0, 0);
  TEST_ASSERT_TRUE (actual);
}

void
test_pita_string_do_not_starts_with (void)
{
  char *string = "/root/onur/misc.json";
  bool actual = pita_string_starts_with (string, "/fool/o", 0, 0);
  TEST_ASSERT_FALSE (actual);
}

void
test_pita_string_has_correct_extension (void)
{
  char *string = "/root/onur/misc.json";
  bool actual = pita_string_has_extension (string, ".json");
  TEST_ASSERT_TRUE (actual);
}

void
test_pita_string_has_incorrect_extension (void)
{
  char *string = "/root/onur/golang.json";
  bool actual = pita_string_has_extension (string, ".toml");

  TEST_ASSERT_FALSE (actual);
}

void
test_pita_string_empty (void)
{
  bool actual = pita_string_empty ("");
  TEST_ASSERT_TRUE (actual);
}

void
test_pita_string_not_empty (void)
{
  bool actual = pita_string_empty ("Vai Corinthians");
  TEST_ASSERT_FALSE (actual);
}
