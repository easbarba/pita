/*
 * Pita is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pita is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pita. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

void test_pita_string_starts_with (void);
void test_pita_string_do_not_starts_with (void);
void test_pita_string_has_correct_extension (void);
void test_pita_string_has_incorrect_extension (void);
void test_pita_string_empty (void);
void test_pita_string_not_empty (void);
