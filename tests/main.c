/*
 * Pita is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pita is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pita. If not, see <https://www.gnu.org/licenses/>.
 */

#include <unity.h>

#include "pita_file_tests.h"
#include "pita_path_tests.h"
#include "pita_string_tests.h"

int
main (void)
{
  UNITY_BEGIN ();

  // STRING
  RUN_TEST (test_pita_string_has_correct_extension);
  RUN_TEST (test_pita_string_has_incorrect_extension);
  RUN_TEST (test_pita_string_starts_with);
  RUN_TEST (test_pita_string_do_not_starts_with);
  RUN_TEST (test_pita_string_not_empty);
  RUN_TEST (test_pita_string_empty);

  // PATH
  RUN_TEST (test_pita_path_stem);
  RUN_TEST (test_pita_path_suffix);
  RUN_TEST (test_pita_path_is_empty);
  RUN_TEST (test_pita_path_is_cwd_dot);
  RUN_TEST (test_pita_path_is_parent_dot);

  // FILE
  RUN_TEST (test_pita_file_exists);
  RUN_TEST (test_pita_file_is_link);
  RUN_TEST (test_pita_file_link_is_dangling);
  RUN_TEST (test_pita_file_link_is_not_dangling);

  return UNITY_END ();
}
