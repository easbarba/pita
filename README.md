<!--
Pita is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Pita is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Pita. If not, see <https://www.gnu.org/licenses/>.
-->

# Pita | C23
The awesomeness of higher-level languages standard libraries, but for C.

`Pita` attempts to bring to C power-users the well-thought API of standard libraries of higher-level language such as Python, Ruby or Guile Scheme, as a shared library.

## USAGE

## INSTALLATION

### Meson

Meson makes it all too easy and nice, just add a `pita.wrap` file in the ./subprojects containing:

``` sh
[wrap-git]
directory=pita
url=https://gitlab.com/easbarba/pita
revision=head
```

Then you in your `meson.build` use it as dependency:

    dependency('pita, fallback: ['pita, 'pita_dep'])

## DEVELOPMENT

`Pita` requires a [C23](https://gcc.gnu.org/) compiler, and [Meson](https://mesonbuild.com/), then just run `make clean all`, and executable file will be placed at `$PWD/.build/onur`.

Dependencies: [GNU GCC](https://gcc.gnu.org/) - [GNU Make](https://www.gnu.org/software/make/) - [Podman ](https://podman.io/).

Tip: A clean install without messing around your system is easily achievable with [GNU Guix](https://guix.gnu.org/manual/devel/en/html_node/Invoking-guix-shell.html): `guix shell --check`.

It may suit you better running the tests in a isolated environment with containers, that can be done so:

    docker run --rm -it $(docker build -qf Containerfile.run)

or:

    podman build https://gitlab.com/easbarba/onur/-/raw/main/Containerfile.dev --tag onur:latest
    podman run --rm -it onur:latest

## LICENSE

[GNU GENERAL PUBLIC LICENSE Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)
