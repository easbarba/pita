/*
 * Pita is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pita is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pita. If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <stdbool.h>

bool pita_file_is_link (const char *);
bool pita_file_is_empty (const char *);
bool pita_file_is_regular (const char *);
bool pita_file_is_directory (const char *);
bool pita_file_link_dangling (const char *);
bool pita_file_exists (const char *);
