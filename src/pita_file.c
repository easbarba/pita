/*
 * Pita is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pita is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pita. If not, see <https://www.gnu.org/licenses/>.
 */

#define _DEFAULT_SOURCE // Enable POSIX functions (including lstat)

#include <ctype.h>
#include <errno.h>
#include <fcntl.h>
#include <linux/limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "pita_file.h"

// check if file or directory file exist
bool
pita_file_exists (const char *filepath)
{
  if (filepath == nullptr)
    return false;

  // empty string
  if (strlen (filepath) == 0)
    return false;

  int fd = open (filepath, O_NOFOLLOW);
  if (fd == -1)
    return false;

  if (errno == EEXIST)
    return false;

  return true;
}

bool
pita_file_is_link (const char *filepath)
{
  struct stat sb;
  int ret = lstat (filepath, &sb);
  if (ret != 0)
    {
      fprintf (stderr, "error: unable to get information of link: %s\n",
               filepath);
      exit (EXIT_SUCCESS);
    }

  if (!S_ISLNK (sb.st_mode))
    return false;

  return true;
}

bool
pita_file_link_dangling (const char *filepath)
{
  if (!pita_file_is_link (filepath))
    return false;

  if (pita_file_exists (filepath))
    return false;

  return true;
}

char *
pita_path_location (const char *filepath)
{
  /* free (executable_path); */
  char *path_list_raw = getenv ("PATH");
  if (path_list_raw == nullptr)
    {
      fprintf (stderr, "error: unable to get PATH environment variable");
      return nullptr;
    }

  char *path_env_dup = (char *)malloc (strlen (path_list_raw) + 1);
  if (path_env_dup == nullptr)
    {
      fprintf (stderr, "error: unable to alocation memory at path_env_dup\n");
      return nullptr;
    }

  strcpy (path_env_dup, path_list_raw);

  char *path_list = strtok (path_env_dup, ":");
  while (path_list != nullptr)
    {
      // +2 for '/' and '\0'
      size_t path_length = strlen (path_list) + strlen (filepath) + 2;
      char *full_path = (char *)malloc (path_length);
      if (full_path == nullptr)
        {
          free (path_env_dup);
          fprintf (stderr,
                   "error: unable to malloc at find_executable_path()");
          return nullptr;
        }

      size_t ret
        = snprintf (full_path, path_length, "%s/%s", path_list, filepath);
      if (ret == false)
        {
          fprintf (stderr, "error: unable to check executable location: %s",
                   filepath);
          return nullptr;
        }

      if (pita_file_exists (full_path) && access (full_path, X_OK) == 0)
        return full_path;

      free (full_path);
      path_list = strtok (nullptr, ":");
    }

  free (path_env_dup);
  return nullptr;
}

bool
pita_file_is_empty (const char *filepath)
{
  FILE *file = fopen (filepath, "r");
  if (file == nullptr)
    {
      fprintf (stderr, "error: could not open configuration file: %s!\n",
               filepath);
      return false;
    }

  int ch;
  while ((ch = fgetc (file)) != EOF)
    {
      if (!isspace (ch))
        {
          fclose (file);
          return false;
        }
    }

  fclose (file);

  return true;
}

bool
pita_file_is_regular (const char *filepath)
{
  struct stat filestat;
  if (stat (filepath, &filestat) == -1)
    {
      fprintf (stderr, "error: cannot read file %s: %s\n", filepath,
               strerror (errno));
      return 0;
    }

  if (!S_ISREG (filestat.st_mode))
    return false;

  return true;
}

bool
pita_file_is_directory (const char *filepath)
{
  struct stat filestat;

  if (stat (filepath, &filestat) == -1)
    {
      fprintf (stderr, "error: cannot read file %s: %s\n", filepath,
               strerror (errno));
      return 0;
    }

  if (!S_ISDIR (filestat.st_mode))
    return false;

  return true;
}
