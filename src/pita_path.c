/*
 * Pita is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pita is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pita. If not, see <https://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE

#include <linux/limits.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>

#include "pita_path.h"

char *
pita_path_join (const char *root, ...)
{
  va_list args;
  va_start (args, root);
  char path_sep[2] = "/";

  const char *component;
  size_t len = strlen (root) + 1;
  char *result = malloc (PATH_MAX);
  if (!result)
    {
      fprintf (stderr,
               "error: allocating memory to result in pita_path_join()");
      return nullptr;
    }

  strcpy (result, root);

  while ((component = va_arg (args, const char *)) != nullptr)
    {
      if (result[strlen (result) - 1] != '/')
        {
          strcat (result, path_sep);
          len++;
        }

      // Check buffer overflow
      if (len + strlen (component) >= PATH_MAX)
        {
          fprintf (stderr, "error: path length exceeds PATH_MAX\n");
          free (result);
          va_end (args);

          return nullptr;
        }

      strcat (result, component);
      len += strlen (component);
    }

  va_end (args);

  return result;
}

// simpler version of join path file root + filename only.
char *
pita_path_join_simple (const char *root, const char *filename)
{
  unsigned long result_len = strlen (root) + 1 + strlen (filename) + 1;
  char *result = (char *)malloc (result_len);
  if (result == nullptr)
    {
      fprintf (stderr,
               "error: could not allocate memory in path_join_simple()");
      return nullptr;
    }

  size_t ret = snprintf (result, result_len, "%s/%s", root, filename);
  if (ret == false)
    {
      fprintf (stderr, "error: unable to TODO absolute path!");
      return nullptr;
    }

  return result;
}

//  base name without the extension
char *
pita_path_stem (const char *filepath)
{
  if (filepath == nullptr)
    return nullptr;

  // empty string
  if (strlen (filepath) == 0)
    return nullptr;

  char *filepath_dup = strdup (filepath);
  if (filepath_dup == nullptr)
    {
      fprintf (stderr, "error: strdup() has gone wrong!");
      return nullptr;
    }

  const char *filename = pita_path_suffix (filepath_dup);
  char *last_dot = strrchr (filename, '.');
  if (last_dot == nullptr)
    {
      free (filepath_dup);
      return nullptr;
    }

  size_t result_len = last_dot - filename;
  char *result = malloc (result_len + 1);
  if (result == nullptr)
    {
      fprintf (stderr, "error: could not allocate memory");
      free (result);
      free (filepath_dup);
      return nullptr;
    }

  strncpy (result, filename, result_len);
  result[result_len] = '\0';

  free (filepath_dup); // Free the duplicated string

  return result;
}

// return filepath extension
char *
pita_path_suffix (const char *filepath)
{
  if (filepath == nullptr)
    return nullptr;

  char *filepath_dup = strdup (filepath);
  if (filepath_dup == nullptr)
    {
      fprintf (stderr, "error: string duplication failed.");
      return nullptr;
    }

  char *result = basename (filepath_dup);
  return result;
}

// is current working directory dot?
bool
pita_path_is_cwd_dot (const char *filepath)
{
  if (filepath == nullptr)
    return false;

  // empty string
  if (strlen (filepath) == 0)
    return false;

  char *filepath_dup = strdup (filepath);
  if (filepath_dup == nullptr)
    {
      fprintf (stderr, "error: string duplication failed.");
      return false; // TODO: exit?
    }

  char *filepath_basename = basename (filepath_dup);
  if (!filepath_basename)
    {
      fprintf (stderr, "error: unable to get basename: %s\n", filepath);
      return false;
    }

  unsigned long fb_len = strlen (filepath_basename);
  if (strncmp (filepath_basename, ".", fb_len) != 0)
    return false;

  return true;
}

// is parent directory dot
bool
pita_path_is_parent_dot (const char *filepath)
{
  if (filepath == nullptr)
    return false;

  // empty string
  if (strlen (filepath) == 0)
    return false;

  char *filepath_dup = strdup (filepath);
  if (filepath_dup == nullptr)
    {
      fprintf (stderr, "error: string duplication failed.");
      return false; // TODO: exit?
    }

  char *filepath_basename = basename (filepath_dup);
  if (filepath_basename == nullptr)
    {
      fprintf (stderr, "error: unable to get basename: %s\n", filepath);
      return false;
    }

  unsigned long fb_len = strlen (filepath_basename);
  if (strncmp (filepath_basename, "..", fb_len) != 0)
    return false;

  return true;
}

/* char * */
/* pita_absolute_path (char *root, char *current_file) */
/* { */
/*   char *result = malloc (sizeof (char) + 1); */
/*   if (result == nullptr) */
/*     { */
/*       fprintf (stderr, "Error: Unable to allocate memory dinamically at " */
/*               "abs_path()"); */
/*       return nullptr; */
/*     } */

/*   int ret = snprintf (result, sizeof (root) + sizeof (result) + 1, "%s/%s",
 */
/*                       root, current_file); */
/*   if (ret == false) */
/*     { */
/*       perror ("Error: creating absolute path!"); */
/*       return nullptr; */
/*     } */

/*   return result; */
/* } */
