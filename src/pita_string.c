/*
 * Pita is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pita is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pita. If not, see <https://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pita_string.h"

bool
pita_string_starts_with (const char *string, const char *prefix,
                         const int start, const int end)
{
  (void)start;
  (void)end;

  int ret = strncmp (string, prefix, strlen (prefix));
  if (ret != 0)
    return false;

  return true;
}

bool
pita_string_ends_with (const char *string, const char *prefix, const int start,
                       const int end)
{
  (void)string;
  (void)prefix;
  (void)start;
  (void)end;

  return true;
}

char *
pita_string_trim (const char *string)
{
  if (string == NULL)
    return NULL;

  // Find the first non-whitespace character from the beginning
  const char *start = string;
  while (isspace (*start))
    {
      start++;
    }

  // Find the last non-whitespace character from the end
  const char *end = string + strlen (string) - 1;
  while (end > start && isspace (*end))
    {
      end--;
    }

  // Calculate the length of the trimmed string
  size_t length = (end - start) + 1;

  // Allocate memory for the trimmed string
  char *trimmed_str = (char *)malloc (length + 1);
  if (trimmed_str == NULL)
    {
      perror ("malloc");
      exit (EXIT_FAILURE);
    }

  // Copy the trimmed substring to the new string
  strncpy (trimmed_str, start, length);
  trimmed_str[length] = '\0';
  return trimmed_str;
  /* char *string_dup = strdup (string); */
  /* if (string_dup == NULL) */
  /*   { */
  /*     free (string_dup); */
  /*     fprintf (stderr, "Error: strdup() gone wrong!"); */
  /*     return NULL; */
  /*   } */

  /* return g_strstrip (string_dup); */
}

bool
pita_string_has_extension (const char *filepath, const char *extension)
{
  if (!filepath || !extension)
    return false;

  if (strlen (filepath) < strlen (extension))
    return false;

  const char *ext_position = strstr (filepath, extension);
  if (ext_position == NULL)
    return false;

  int ret = strcmp (ext_position, extension);
  if (ret != 0)
    return false;

  return true;
}

bool
pita_string_empty (char *string)
{
  if (string[0] != '\0')
    return false;

  return true;
}
