/*
 * Pita is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Pita is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Pita. If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>

#include "pita_path.h"
#include "pita_system.h"

void
pita_run_cmd (char *args[], bool verbose)
{
  char *exec_location = pita_path_location (args[0]);
  if (exec_location == nullptr)
    {
      fprintf (stderr, "Error: '%s' not found in your PATH\n", args[0]);
      return;
    }

  __pid_t pid = fork ();
  if (pid < 0)
    {
      fprintf (stderr, "fork failed!");
      return;
    }

  if (pid == 0)
    {
      int ret = execve (exec_location, args, nullptr);
      if (ret == -1)
        {
          fprintf (stderr, "execve failed");
          return;
        }
    }

  int status;
  __pid_t pidf = wait (&status);
  if (pidf == -1)
    {
      fprintf (stderr, "wait failed");
      return;
    }

  if (verbose)
    fprintf (stderr, "Child process with PID %d terminated\n", (int)pidf);

  if ((WIFEXITED (status) > 0) && verbose)
    fprintf (stderr, "Child exited normally with status: %d\n",
             WEXITSTATUS (status));

  if ((WIFSIGNALED (status) > 0) && verbose)
    fprintf (stderr, "Child terminated by signal: %d\n", WTERMSIG (status));

  /* free (args); */
}
