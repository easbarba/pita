# Pita is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pita is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pita. If not, see <https://www.gnu.org/licenses/>.

# DEPS: gcc meson ninja muon coreutils valgrind indent splint cunit

.DEFAULT_GOAL: test
RUNNER := podman

PREFIX     ?= ${HOME}/.local/bin
NAME       = pita
VERSION    := $(shell cat version)
IMAGES_REGISTRY := registry.gitlab.com
IMAGE_NAME := ${IMAGES_REGISTRY}/${USER}/${NAME}:${VERSION}

RM         = rm -rf
BUILDDIR = ./build

# ------------------------------------ TASKS

.PHONY: setup
setup:
	CC=gcc meson setup build --wipe

.PHONY: all
all: setup
	meson compile -C $(BUILDDIR)

.PHONY: build
build:
	meson compile -C $(BUILDDIR)

.PHONY: test
test:
	${RUNNER} run --rm -it  ${IMAGE_NAME}

.PHONY: muon
muon:
	CC=gcc muon-meson setup $(BUILDDIR)
	ninja -C $(BUILDDIR)

.PHONY: clean
clean:
	@$(RM) $(BUILDDIR)
	@$(RM) .cache

.PHONY: install
install:
	cp -v ${BUILDDIR}/src/${NAME} ${PREFIX}/${NAME}

.PHONY: uninstall
uninstall:
	rm ${PREFIX}/${NAME}

.PHONY: format
format:
	indent -l 80 -gnu -nut -bl  src/*.c
	muon-meson fmt -i ./meson.build

.PHONY: lint
lint:
	splint -preproc -unrecog -warnposix src/*.c

.PHONY: leaks
leaks:
	valgrind --leak-check=full \
		 --show-leak-kinds=all \
		 --track-origins=yes \
		 --verbose \
		 $(BUILDDIR)/src/onur --grab
		 # --log-file=valgrind-output \

# ------------------------------------ ACTIONS

.PHONY: usage
usage:
	$(BUILDDIR)/src/$(NAME) --help

.PHONY: grab
grab:
	$(BUILDDIR)/src/$(NAME) --grab

.PHONY: archive
archive:
	$(BUILDDIR)/src/$(NAME) --archive meh,foo,bar

.PHONY: filter
filter:
	$(BUILDDIR)/src/$(NAME) --filter golang --grab

# ------------------------------------ ACTIONS

.PHONY: image.build
image.build:
	$(RUNNER) build -f Containerfile --tag  ${IMAGE_NAME}

.PHONY: image.publish
image.publish:
	${RUNNER} push ${IMAGE_NAME}

.PHONY: image.tests
image.tests:
	${RUNNER} run --rm -it ${IMAGE_NAME}

.PHONY: image.repl
image.repl:
	${RUNNER} run --rm -it ${IMAGE_NAME} bash
