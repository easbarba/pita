#!/usr/bin/env bash

# Pita is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Pita is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Pita. If not, see <https://www.gnu.org/licenses/>.

mkdir -pv /root/onur

cp -v ./examples/* /root/onur

touch /root/onur/emptyfile.json
ln -sf /root/nonexistentfile /root/onur/dangling_link.json
ln -sf /root/onur/etc.json /root/onur/etc_link.json
